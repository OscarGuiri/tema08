package com.oscarmorton.tema08.Ejer4;

public class Ejer4 {
    public static void main(String[] args) {
        //Creo dosobjeto punto y les pongo una valor. 
        Punto p1 = new Punto();
        p1.setX(2.0);
        p1.setY(4.0);
        System.out.println("La coordenada es:" + p1.getX() + ", " + p1.getY());
        Punto p2 = new Punto();
        p2.setX(1.0);
        p2.setY(5.0);
        System.out.println("La coordenada es:" + p2.getX() + ", " + p2.getY());
        System.out.printf("La distancia entre los dos puntos son: %.2f ", p1.calcularDistancia(p2));
    }
}
