public class Alumnos{
    private String nia;
    private String nombre;
    private String apellido;
    private String nacimiento;
    private String grupo;
    private String telefono;

    public Alumnos(String nia, String nombre, String apellido, String nacimiento, String grupo, String telefono){
        this.nia = nia;
        this.nombre = nombre;
        this.apellido = apellido;
        this.nacimiento = nacimiento;
        this.grupo = grupo;
        this.telefono = telefono;

        
    }
    

    //*********************** GET AND SET ***********************/
    /**
     * @param nia the nia to set
     */
    public void setNia(String nia) {
        this.nia = nia;
    }
    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    /**
     * @param nacimiento the nacimiento to set
     */
    public void setNacimiento(String nacimiento) {
        this.nacimiento = nacimiento;
    }
    /**
     * @param grupo the grupo to set
     */
    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }
    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    /**
     * @return the nia
     */
    public String getNia() {
        return nia;
    }
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }
    /**
     * @return the nacimiento
     */
    public String getNacimiento() {
        return nacimiento;
    }
    /**
     * @return the grupo
     */
    public String getGrupo() {
        return grupo;
    }
    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }
    



}   