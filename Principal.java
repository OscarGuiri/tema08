public class Principal {
  public static void main(String[] args) {
    Coche c1 = new Coche(); // Creo los coches como objetos.
    Coche c2 = new Coche();
    //Coche nº 1
    c1.setColor("Rojo");
    c1.setFabricante("Mercedes");
    c1.setMatricula(1234);
    c1.setMetalica(false);
    c1.setModelo("7G");
    // c1.setTipo(); CON DUDA 

    //Coche nº 1
    c2.setColor("Negro");
    c2.setFabricante("BMW");
    c2.setMatricula(7723);
    c2.setMetalica(true);
    c2.setModelo("Serie1");
    // c2.setTipo(); CON DUDA 
    System.out.println("Coche 1:");
    imprimeCoche(c1);
    System.out.println("Coche 2:");
    imprimeCoche(c2);

    //Ahora cambio el color y la matricula del coche 1
    c1.setColor("Azul");
    c1.setMatricula(2352);
    System.out.println("Coche 1 modificado: ");
    imprimeCoche(c1);


  }
  
  

  /**
   * 
   * @param c
   */
  public static void imprimeCoche(Coche c) {
    System.out.println("COLOR: " + c.getColor());
    System.out.println("FABRICANTE: " + c.getFabricante());
    System.out.println("MATRICULA: " + c.getMatricula());
    System.out.println("METALICA: " + c.getMetalica());
    System.out.println("MODELO: " + c.getModelo());
    System.out.println("Tipo: " + c.getTipo());

  }
}