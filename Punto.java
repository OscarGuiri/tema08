package com.oscarmorton.tema08.Ejer4;


public class Punto {
    private double x;
    private double y;
    // Contructor de los dos parametros
    public Punto(double x, double y){
        this.x = x;
        this.y = y;

    }
    //Contructor por defecto
    public Punto(){
        this.x = 0;
        this.y = 0;
    }
    //***GETTERS AND SETTERS***
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
    //Asigno los puntos y realizo el calculo en el variavle dis
    public double calcularDistancia(Punto punto){
        double x1 = x;
        double y1 = y;
        double x2 = punto.getX();
        double y2 = punto.getY();
        double dis=Math.sqrt(Math.pow(x2-x1,2) + Math.pow(y2-y1,2));
        return dis;
        
    }
}
